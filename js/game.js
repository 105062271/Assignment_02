/* Initialize states and start boot state */

var config = {

    width: 880,
    height: 720,
    renderer: Phaser.AUTO,
    parent: 'canvas',       // inject into 'canvas' in index.html
    transparent: false,      // non-transparent background
    antilias: true,         // smooth graphics

}

var game = new Phaser.Game(config);
game.state.add('boot', bootState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('gameover', gameoverState);
game.state.start('menu');

var levels = 1;
var score = 0;