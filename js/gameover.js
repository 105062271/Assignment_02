var gameoverState = {

    preload: function () {

        game.load.image('gameover', 'assets/ui/gameover.png', 372, 83);
        game.load.spritesheet('try_again', 'assets/ui/try_again.png',
            296, 57);
        game.load.spritesheet('back_to_menu', 'assets/ui/back_to_menu.png',
            239, 26);

    },


    create: function () {

        // scrolling bg
        this.bg1 = game.add.image(0, 0, 'background');
        this.bg2 = game.add.image(0, game.height, 'background');

        // gameover image
        var gameoverImage = game.add.image(446, 276, 'gameover');
        gameoverImage.anchor.setTo(0.5);

        // show score
        var font = {
            font: "33px DracuFrankenWolfBB",
            fill: "#dadada",
            fontWeight: "bold",
            align: "left"
        };

        this.levelsText = game.add.text(294, 340, 'level: ' + levels, font);
        this.scoreText = game.add.text(294, 385, 'score: ' + score, font);


        // buttons
        var tryAgain = game.add.button(437, 535, 'try_again',
            this.onTryClicked, this, 1, 0);
        tryAgain.anchor.setTo(0.5, 0.5);

        var backToMenu = game.add.button(439, 608, 'back_to_menu',
            this.onBackClicked, this, 1, 0);
        backToMenu.anchor.setTo(0.5, 0.5);

    },

    update: function () {

        // scroll bg
        this.bg1.y -= 2;
        this.bg2.y -= 2;

        if (this.bg1.y == -game.height) {

            this.bg1.y = game.height;

        } else if (this.bg2.y == -game.height) {

            this.bg2.y = game.height;
        }

    },


    onTryClicked: function () {

        game.state.start('play');

    },


    onBackClicked: function () {

        game.state.start('menu');

    }

}