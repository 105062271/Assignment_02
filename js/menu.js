var menuState = {

    preload: function () {

        game.load.image('background', 'assets/enviroment/hell.png',
            880, 720);
        game.load.image('menu_background',
            'assets/ui/menu_background.png', 880, 720);
        game.load.spritesheet('green_hatter_large',
            'assets/ui/green_hatter_large.png', 440, 740);
        game.load.image('lighter', 'assets/ui/lighter.png',
            177, 191);
        game.load.spritesheet('play', 'assets/ui/play.png', 131, 54);
        game.load.spritesheet('credit', 'assets/ui/credit.png', 139, 30);
        game.load.spritesheet('leaderboard', 'assets/ui/leaderboard.png',
            297, 30);

    },


    create: function () {

        // scrolling bg
        this.bg1 = game.add.image(0, 0, 'background');
        this.bg2 = game.add.image(0, game.height, 'background');

        // menu bg
        game.add.image(0, 0, 'menu_background');

        // character
        var gh_large = game.add.sprite(game.width / 2 + 40, 0,
            'green_hatter_large');
        gh_large.animations.add('swing',
            Phaser.ArrayUtils.numberArray(0, 61), 20, true);
        gh_large.animations.play('swing');

        // lighter
        this.lighter = game.add.sprite(game.width / 2 - 70, 150, 'lighter');
        this.lighter.anchor.setTo(0.5);

        // buttons
        var play = game.add.button(133, 527, 'play',
            this.onPlayClicked, this, 1, 0);
        play.anchor.setTo(0.5, 0.5);

        var credit = game.add.button(194, 596, 'credit',
            this.onCreditClicked, this, 1, 0);
        credit.anchor.setTo(0.5, 0.5);

        var leaderboard = game.add.button(237, 653, 'leaderboard',
            this.onLeaderboardClicked, this, 1, 0);
        leaderboard.anchor.setTo(0.5, 0.5);

    },


    update: function () {

        // rotate lighter
        this.lighter.angle += 3;

        // scroll bg
        this.bg1.y -= 2;
        this.bg2.y -= 2;

        if (this.bg1.y == -game.height) {

            this.bg1.y = game.height;

        } else if (this.bg2.y == -game.height) {

            this.bg2.y = game.height;
        }

    },


    onPlayClicked: function () {

        game.state.start('play');
    },


    onCreditClicked: function () {

        console.log('you clicked credit!');
    },


    onLeaderboardClicked: function () {

        console.log('you clicked leaderboard!');
    }

}