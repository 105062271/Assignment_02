var playState = {

    /***************************/
    /*     State functions     */
    /***************************/
    preload: function () {

        /*** Load levels asset ***/
        game.load.image('platform_normal_rock',
            'assets/enviroment/platform_normal_rock.png', 210, 30);
        game.load.image('platform_cracked_dirt',
            'assets/enviroment/platform_cracked_dirt.png', 210, 30);
        game.load.image('small_cracked_dirt',
            'assets/enviroment/small_cracked_dirt.png', 32, 30);
        game.load.image('platform_spiking_ground',
            'assets/enviroment/platform_spiking_ground.png', 210, 30);
        game.load.spritesheet('spiderweb',
            'assets/enviroment/spiderweb_spritesheet.png', 880, 100);


        /*** Load character asset ***/
        game.load.spritesheet('green_hatter',
            'assets/character/green_hatter.png', 300, 400);


        /*** Load user interface element ***/
        game.load.image('health_bar', 'assets/ui/health_bar.png',
            273, 13);


    },


    create: function () {

        /*** Set up world ***/
        this.BUFFER_HEIGHT = 720;
        this.scrollSpeed = 2.5;
        this.heightCounter = 0;
        game.world.setBounds(0, 0, game.width,
            game.height * 2 + this.BUFFER_HEIGHT);
        game.add.tileSprite(0, 0, game.width,
            game.height * 2 + this.BUFFER_HEIGHT, 'background');
        game.physics.startSystem(Phaser.Physics.P2JS);
        game.physics.p2.gravity.y = 2000;
        


        /*** Create character ***/
        /*-- sprite --*/ 
        this.player = game.add.sprite(game.width / 2, 100,
            'green_hatter');
        //this.player.anchor.setTo(0.3, 0.5);   This doesn't work for animated sprites
        this.player.scale.setTo(0.35);

        /*-- animation --*/
        this.player.animations.add('idle',
            Phaser.ArrayUtils.numberArray(0, 49), 62.5, true);
        this.player.animations.add('run',
            Phaser.ArrayUtils.numberArray(50, 111), 150, true);
        this.player.animations.add('jump_off',
            Phaser.ArrayUtils.numberArray(150, 164), 62.5, false);
        this.player.animations.add('fall',
            Phaser.ArrayUtils.numberArray(200, 261), 62.5, true);
        this.player.animations.add('jump_down',
            Phaser.ArrayUtils.numberArray(300, 336), 350, false);

        /*-- physics --*/
        game.physics.p2.enable(this.player, false);
        this.player.body.damping = 0.8;
        this.player.body.fixedRotation = true;
        this.player.body.clearShapes();     // clear default rectangle shape
        this.player.body.addCapsule(55, 25, 0, 5,
            Phaser.Math.degToRad(90));
        //this.player.body.collideWorldBounds = true;
        this.player.body.onBeginContact.add(this.playerCollide, this);
        this.player.body.onEndContact.add(this.playerNotCollide, this);

        /*-- tweens --*/
        this.playerDamgeTween = game.add.tween(this.player).from(
            { tint: Phaser.Color.RED }, // tint color
            100,                        // duration
            Phaser.Easing.Bounce.out,   // easing function
            false,
            0,
            2,                          // repeat times
            true                        // yo-yo
        );    

        /*-- properties --*/
        this.playerSpeed = 300;
        this.playerJumpSpeed = 750;
        this.playerHealth = 100;
        this.playerHealthSpeed = 2.5    // per second
        this.playerOnGround = false;
        this.playerHittingWeb = false;

        /*-- player states --*/
        this.PLAYERSTATE = {
            IDLE: 1,
            RUNNING: 2,
            JUMPING_OFF: 3,
            FALLING: 4,
            JUMPING_DOWN: 5
        };
        this.playerState = this.PLAYERSTATE.IDLE;


        /*** Setup platform ***/
        /*-- pace --*/
        this.generatePeriod = Phaser.Timer.SECOND * 3;

        this.generateDensityMin = 1;
        this.generateDensityMax = 3;

        this.generateYRange = 150;   // +- range

        /*-- initial probabilities --*/
        this.normalRocksProbability = 0.9;  // make sure the sum is 1
        this.crackedDirtProbability = 0.05;
        this.spikingGroundProbability = 0.05;

        /*-- crackedDirt property --*/
        this.crackedDirtCollapseTime = Phaser.Timer.SECOND * 0.5;

        /*-- spikingGround property --*/
        this.spikingGroundDamage = 33;

        /*-- setup groups --*/
        this.checkPlatforms = game.add.group();
        this.checkPlatforms.physicsBodyType = Phaser.Physics.P2JS;
        this.checkPlatforms.enableBody = true;
        this.checkPlatforms.enableBodyDebug = true;

        this.garbageGroup = game.add.group();

        this.game.world.bringToTop(this.player);

        /*-- generate here --*/
        this.generatePlatform('platform_normal_rock', game.width / 2,
            game.height * 0.9);
        game.time.events.loop(this.generatePeriod, this.generatePlatforms,
            this);


        /*** Set spiderweb ***/
        this.spiderweb = game.add.sprite(game.camera.x, game.camera.y,
            'spiderweb');
        this.spiderweb.animations.add('swing',
            Phaser.ArrayUtils.numberArray(0, 61), 15, true);
        this.spiderweb.animations.play('swing');
        this.spiderweb.fixedToCamera = true;

        this.spiderwebDamge = 33;


        /*** Key inputs ***/
        this.keyRecord = {
            'LEFT': false,
            'RIGHT': false,
            'SPACEBAR': false
        }


        /*** Setup UI ***/
        /*-- level text --*/
        var fontObj1 = {
            font: "28px DracuFrankenWolfBB",
            fill: "#FFFFFF",
            stroke: "#000000",
            strokeThickness: 5,
            align: "center"
        };
        var fontObj2 = {
            font: "50px DracuFrankenWolfBB",
            fill: "#FFFFFF",
            stroke: "#000000",
            strokeThickness: 5,
            align: "center"
        };

        this.levelsTextPrefix = game.add.text(36, game.height - 70,
            'Underground LV:', fontObj1);
        this.levelsText = game.add.text(this.levelsTextPrefix.right,
            this.levelsTextPrefix.y - 17, '1', fontObj2);
        this.levelsTextPrefix.fixedToCamera = true;
        this.levelsText.fixedToCamera = true;

        this.levels = 1;

        this.score = 0;

        /*-- health bar --*/
        this.healthBar = game.add.image(36, game.height - 30,
            'health_bar');
        this.healthBar.fixedToCamera = true;
        
    },


    update: function () {

        this.handleInput();
        this.updatePlayer();
        this.scrollAndResize();
        //console.log("vY = " + this.player.body.velocity.y);

    },



    /***************************/
    /*  Operational functions  */
    /***************************/
    handleInput: function () {

        if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT))
            this.keyRecord['LEFT'] = true;
        else
            this.keyRecord['LEFT'] = false;

        if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT))
            this.keyRecord['RIGHT'] = true;
        else
            this.keyRecord['RIGHT'] = false;

        if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
            this.keyRecord['SPACEBAR'] = true;
        else
            this.keyRecord['SPACEBAR'] = false;

    },


    updatePlayer: function () {

        /*** Handle state ***/
        switch (this.playerState) {

            case this.PLAYERSTATE.IDLE:
                /*-- IDLE to RUNNING --*/
                if (this.keyRecord['LEFT'] && !this.keyRecord['RIGHT']
                    && this.playerOnGround) {

                    this.playerState = this.PLAYERSTATE.RUNNING;

                } else if (this.keyRecord['RIGHT']
                    && !this.keyRecord['LEFT']
                    && this.playerOnGround) {

                    this.playerState = this.PLAYERSTATE.RUNNING;

                }
                /*-- IDLE to JUMPING_OFF --*/
                else if ((this.keyRecord['SPACEBAR']
                    && this.playerOnGround)) {

                    this.playerState = this.PLAYERSTATE.JUMPING_OFF;
                    this.player.body.velocity.y -= this.playerJumpSpeed;
                    this.playerOnGround = false;

                }

                /*-- IDLE to FALLING --*/
                if (!this.playerOnGround) {

                    this.playerState = this.PLAYERSTATE.FALLING;

                }
                break;

            case this.PLAYERSTATE.RUNNING:
                /*-- RUNNING to IDLE: not pressed --*/
                if (!this.keyRecord['LEFT'] && !this.keyRecord['RIGHT']) {

                    this.playerState = this.PLAYERSTATE.IDLE;

                }
                /*-- RUNNING to IDLE: presse both --*/
                else if (this.keyRecord['LEFT']
                    && this.keyRecord['RIGHT']) {

                    this.playerState = this.PLAYERSTATE.IDLE;

                }
                /*-- RUNNING to JUMPING_OFF --*/
                else if (this.keyRecord['SPACEBAR']
                      && this.playerOnGround) {

                    this.playerState = this.PLAYERSTATE.JUMPING_OFF;
                    this.player.body.velocity.y -= this.playerJumpSpeed;
                    this.playerOnGround = false;

                }

                /*-- RUNNING to FALLING --*/
                if (!this.playerOnGround) {

                    this.playerState = this.PLAYERSTATE.FALLING;

                }
                break;

            case this.PLAYERSTATE.JUMPING_OFF:
                /*-- JUMING_OFF to FALLING --*/
                this.player.animations.currentAnim.onComplete.add(
                    function () {

                        this.playerState = this.PLAYERSTATE.FALLING;

                    }, this
                );
                break;

            case this.PLAYERSTATE.FALLING:
                /*-- FALLING to JUMPING_DOWN --*/
                if (this.playerOnGround) {
  
                    this.playerState = this.PLAYERSTATE.JUMPING_DOWN;

                }
                break;

            case this.PLAYERSTATE.JUMPING_DOWN:
                this.player.animations.currentAnim.onComplete.add(
                    function () {
                        /*-- JUMING_DOWN to RUNNING --*/
                        if (this.keyRecord['LEFT']
                            && !this.keyRecord['RIGHT']) {

                            this.playerState = this.PLAYERSTATE.RUNNING;

                        } else if (!this.keyRecord['LEFT']
                            && this.keyRecord['RIGHT']) {

                            this.playerState = this.PLAYERSTATE.RUNNING;

                        }
                        /*-- JUMING_DOWN to IDLE --*/
                        else {

                            this.playerState = this.PLAYERSTATE.IDLE;

                        }
                    }, this
                );
                /*-- JUMPING_DOWN to JUMPING_OFF --*/
                if (this.keyRecord['SPACEBAR'] && this.playerOnGround) {

                    this.playerState = this.PLAYERSTATE.JUMPING_OFF;
                    this.player.body.velocity.y -= this.playerJumpSpeed;
                    this.playerOnGround = false;

                }
                /*-- JUMPING_DOWN to FALLING --*/
                if (!this.playerOnGround) {

                    this.playerState = this.PLAYERSTATE.FALLING;

                }
                break;

        }


        /*** Change player by state ***/
        /*-- velocity: x --*/
        switch (this.playerState) {

            case this.PLAYERSTATE.IDLE:
                this.player.body.velocity.x = 0;
                break;

            case this.PLAYERSTATE.RUNNING:
                if (this.keyRecord['LEFT'])
                    this.player.body.velocity.x = -this.playerSpeed;
                else
                    this.player.body.velocity.x = this.playerSpeed;
                break;

            case this.PLAYERSTATE.JUMPING_OFF:
            case this.PLAYERSTATE.FALLING:
            case this.PLAYERSTATE.JUMPING_DOWN:
                if (this.keyRecord['LEFT'])
                    this.player.body.velocity.x = -this.playerSpeed;
                else if (this.keyRecord['RIGHT'])
                    this.player.body.velocity.x = this.playerSpeed;
                else
                    this.player.body.velocity.x = 0;
                break;

        }

        /*-- animation --*/
        switch (this.playerState) {

            case this.PLAYERSTATE.IDLE:
                this.player.animations.play('idle');
                break;

            case this.PLAYERSTATE.RUNNING:
                this.player.animations.play('run');
                break;

            case this.PLAYERSTATE.JUMPING_OFF:
                this.player.animations.play('jump_off');
                break;

            case this.PLAYERSTATE.FALLING:
                this.player.animations.play('fall');
                break;

            case this.PLAYERSTATE.JUMPING_DOWN:
                this.player.animations.play('jump_down');
                break;

        }

        /*-- sprite direction --*/
        if (this.keyRecord['RIGHT'] && !this.keyRecord['LEFT']
            && this.player.scale.x > 0)
            this.player.scale.x *= -1;
        if (this.keyRecord['LEFT'] && !this.keyRecord['RIGHT']
            && this.player.scale.x < 0)
            this.player.scale.x *= -1;

        /*-- update onGround --*/
        /*var vY = this.player.body.velocity.y;
        if (vY < 0.1 && vY > -0.1) {
            this.playerOnGround = true;
        } else {
            this.playerOnGround = false;
        }*/

        /*-- check for bounds --*/
        if (this.player.y - this.player.height / 2 <=
            game.camera.y + 10) {

            if (!this.playerHittingWeb) {

                this.playerHittingWeb = true;
                if (!this.playerDamgeTween.isRunning) {

                    this.updatePlayerHealth(-this.spiderwebDamge);

                    this.playerDamgeTween.start();

                    this.turnOffPlayerShapes(Phaser.Timer.SECOND * 0.6);
                }

            }

        } else {

            this.playerHittingWeb = false;
        }

        if (this.player.y - this.player.height / 2 >=
            game.camera.y + game.height) {

            this.updatePlayerHealth(-100);
            this.gameOver();
        }

        /*-- heal by default --*/
        this.updatePlayerHealth(this.playerHealthSpeed / 60);

    },


    scrollAndResize: function () {

        game.camera.y += this.scrollSpeed;

        /*** Load New Area ***/
        if (game.camera.y + game.height * 2 >=
            game.world.bounds.bottom) {

            game.world.setBounds(0, game.camera.y,
                game.width, game.height * 2 + this.BUFFER_HEIGHT); 
            var bg = game.add.tileSprite(0, game.camera.y +
                game.height * 2 - 1, game.width, this.BUFFER_HEIGHT,
                'background');
            game.world.sendToBack(bg);

        }


        /*** Check Level ***/
        this.heightCounter += this.scrollSpeed;

        if (this.heightCounter >= game.height) {

            this.heightCounter = 0;

            this.levels += 1;
            this.levelsText.setText(this.levels);
            this.score += 1753;

            // if achieve certain levels
            // 1.change text color
            // 2.change probabilities
            if (this.levels == 20) {

                this.levelsText.addColor('#32c74a', 0);

                this.normalRocksProbability = 0.7;
                this.crackedDirtProbability = 0.22;
                this.spikingGroundProbability = 0.08;

            } else if (this.levels == 40) {

                this.levelsText.addColor('#c7c532', 0);

                this.normalRocksProbability = 0.65;
                this.crackedDirtProbability = 0.3;
                this.spikingGroundProbability = 0.05;

            } else if (this.levels == 60) {

                this.levelsText.addColor('#c76a32', 0);

                this.normalRocksProbability = 0.65;
                this.crackedDirtProbability = 0.05;
                this.spikingGroundProbability = 0.3;

            } else if (this.levels == 80) {

                this.levelsText.addColor('#c71818', 0);

                this.normalRocksProbability = 0.6;
                this.crackedDirtProbability = 0.2;
                this.spikingGroundProbability = 0.2;

            } else if (this.levels == 100) {

                this.levelsText.addColor('#71078f', 0);

                this.normalRocksProbability = 0.4;
                this.crackedDirtProbability = 0.5;
                this.spikingGroundProbability = 0.1;

            } else if (this.levels == 120) {

                console.log('You\'re theeeeere!');

            }
            
        }

    },


    playerCollide: function(bodyA, bodyB, shapeA, shapeB, equation) {

        if (bodyA) {

            if (   this.checkPlatforms.contains(bodyA.sprite)
                || this.garbageGroup.contains(bodyA.sprite)) {

                this.playerOnGround = true;
                this.score += 666;

            }
            //console.log('collides!');

            if (bodyA.sprite.key == 'platform_cracked_dirt') {

                this.hitCrackedDirt(bodyA.sprite);

            } else if (bodyA.sprite.key == 'platform_spiking_ground') {

                this.hitSpikingGround(bodyA.sprite);

            }

        } else {

            // player hit world bounds
            //game.state.start('play');
            
        }

    },


    playerNotCollide: function (bodyA, bodyB, shapeA, shapeB) {

        if (bodyA) {

            if (this.checkPlatforms.contains(bodyA.sprite)
                || this.garbageGroup.contains(bodyA.sprite)) {

                this.playerOnGround = false;
            }

        } else {

            console.log('player endContact');
        }

    },


    generatePlatforms: function () {

        // clear prev to-check group
        this.checkPlatforms.moveAll(this.garbageGroup);
        //console.log("cleaned to-check group, the length is now: ", this.checkPlatforms.length);

        // pick quantity to gen
        var n = game.rnd.integerInRange(this.generateDensityMin,
            this.generateDensityMax);
        //console.log("gen " + n + ":");
        // probability ranges
        var range1 = this.normalRocksProbability * 1000;
        var range2 = range1 + this.crackedDirtProbability * 1000;
        var range3 = range2 + this.spikingGroundProbability * 1000;

        for (var i = 0; i < n; i++) {

            // pick platform type according to their probabilities
            var key;
            
            var rndInt = game.rnd.integerInRange(1, 1000);
            //console.log(rndInt);
            if (rndInt <= range1)
                key = 'platform_normal_rock';
            else if (rndInt <= range2)
                key = 'platform_cracked_dirt';
            else if (rndInt <= range3)
                key = 'platform_spiking_ground';
            //console.log("pick " + key);

            var leftBound = 0.5 * this.cache.getImage(key).width;
            var rightBound = game.width - leftBound;

            // pick y
            var offset = game.rnd.integerInRange(-this.generateYRange,
                this.generateYRange);
            var y = game.camera.y + game.height
                + this.generateYRange + offset;

            // pick x without repeated
            // if overlay, pick new one, up to 10 times
            var rep_count = 0;
            while (1) {

                rep_count += 1;
                var x = game.rnd.realInRange(leftBound, rightBound);
                //console.log("now picking x: " + x);

                if (this.checkOverlayPlatform(key, x, y)) {
                    
                    if (rep_count < 10) {
                        //console.log("overlay, pick next x");
                        continue;
                    } else {
                        //console.log("overlay 10 times, give up gen");
                        break;
                    }

                } else {

                    this.generatePlatform(key, x, y);
                    break;
                }
            }

        }

    },


    checkOverlayPlatform: function (key, x, y) {

        var w = this.cache.getImage(key).width;
        var h = this.cache.getImage(key).height;
        var rect = new Phaser.Rectangle(x - w / 2, y - h / 2, w, h);
        
        var overlay = false;
        for (var i = 0; i < this.checkPlatforms.length; i++) {

            var existSprite = this.checkPlatforms.children[i];
            var existX = existSprite.x;
            var existY = existSprite.y;
            var existW = existSprite.width;
            var existH = existSprite.height;
            var existRect = new Phaser.Rectangle(existX - existW / 2,
                existY - existH / 2, existW, existH);

            //console.log("checking overlay...element " + i);
            //console.log("rect:" + rect);
            //console.log("existRect:" + existRect);
            //console.log("overlap? " + Phaser.Rectangle.intersects(rect, existRect));
            if (Phaser.Rectangle.intersects(rect, existRect)) {

                overlay = true;
                delete rect;
                break;

            } else {

                delete rect;

            }
        }

        return overlay;

    },


    generatePlatform: function (key, x, y) {

        //console.log("generating " + key + " at " + x + ", " + y);

        // create in to-check group
        var sprite = this.checkPlatforms.create(x, y, key);

        // decide physics and change shape if needed
        sprite.body.static = true;

        if (key == 'platform_spiking_ground') {

            //sprite.body.debug = true;
            sprite.body.clearShapes();

            var image = game.cache.getImage(key);
            var w = image.width;
            var h = image.height * 0.68;
            sprite.body.addRectangle(w, h, 0, (image.height - h) / 2);
        }

        // destroy when out of bound
        sprite.checkWorldBounds = true;
        sprite.events.onOutOfBounds.add(this.destroyPlatform, this);
        
    },


    destroyPlatform: function (platform) {

        //platform.parent.remove(platform, true);
        platform.destroy();

        //console.log("destroy platform!");
        //console.log("numbers in garbage group: " + this.garbageGroup.length);

    },


    hitCrackedDirt: function (sprite) {

        game.time.events.add(this.crackedDirtCollapseTime, function () {

            // because player handler won't invoke endContact when destroy dirt
            // check reversely
            // if still on that dirt set onGround to false manually
            if (sprite.body) {

                sprite.body.onEndContact.add(function (bodyA, bodyB,
                    shapeA, shapeB, equation) {

                    if (bodyA.sprite.key == 'green_hatter') {

                        this.playerOnGround = false;
                    }

                }, this);

            }

            // destroy sprite
            this.destroyPlatform(sprite);

            // cracked apart effect
            var emitter = game.add.emitter(sprite.x, sprite.y, 50);
            emitter.width = sprite.width;
            emitter.height = sprite.height;
            emitter.makeParticles('small_cracked_dirt');
            emitter.gravity = 1000;
            emitter.minParticleScale = 0.05;
            emitter.maxParticleScale = 0.75;
            emitter.explode(Phaser.Timer.SECOND * 2);

        }, this);

    },


    hitSpikingGround: function () {

        this.updatePlayerHealth(-this.spikingGroundDamage);

        if (!this.playerDamgeTween.isRunning) {

            this.playerDamgeTween.start();
        }
                      
    },


    updatePlayerHealth: function (change) {

        if (change == 0) {

            return;

        } else {

            this.playerHealth += change;
            this.healthBar.width = this.playerHealth / 100 *
                game.cache.getImage('health_bar').width;

            if (change < 0)
                this.score += change * 10;

            if (this.playerHealth >= 100) {

                this.playerHealth = 100;
                this.healthBar.width = this.playerHealth / 100 *
                    game.cache.getImage('health_bar').width;

                return;
            }

            if (this.playerHealth <= 0) {

                this.playerHealth = 0;
                this.healthBar.width = this.playerHealth / 100 *
                    game.cache.getImage('health_bar').width;

                this.gameOver();

            }

        }

    },


    turnOffPlayerShapes: function (time) {

        this.player.body.clearShapes();

        game.time.events.add(time, function () {

            this.player.body.addCapsule(55, 25, 0, 5,
                Phaser.Math.degToRad(90));

        }, this);

    },


    gameOver: function () {

        levels = this.levels;
        score = this.score;

        game.world.setBounds(0, 0, game.width,
            game.height * 2 + this.BUFFER_HEIGHT);
        game.camera.y = 0;


        game.state.start('gameover');
    }

};